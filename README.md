<h1>Drive Car</h1>
<h3>With HTML, CSS and Javascript</h3>
<br>
<hr>

<p>A simple single page using HTML, CSS and Javascript.</p>

<ul>
	<li>w key: Turn headlights ON / OFF</li>
	<li>enter key: starts / stops the car</li>
</ul>

<br><br>
<hr>

<p><em>Note: Not my creation, learned and coded from youtube tutorials.</em></p>